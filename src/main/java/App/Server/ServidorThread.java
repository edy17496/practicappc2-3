package App.Server;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public class ServidorThread extends Thread {


    public transient static final int X = 2 * 1000;
    public transient static final int Y = 4 * 1000;

    private DatagramPacket datagramPacket;
    private DatagramSocket datagramSocket;


    public ServidorThread(DatagramSocket datagramSocket, DatagramPacket datagramPacket) {
        this.datagramSocket = datagramSocket;
        this.datagramPacket = datagramPacket;
    }

    @Override
    public void run() {
        enviarMensaje(datagramPacket);
    }

    public void comandoHelp() {
        System.out.print("COMANDOS A UTILIZAR\n"
                + "\tDETENER SERVIDOR [EXIT]\n");
    }

    public void enviarMensaje(DatagramPacket datagramPacket) {
        try {
            Thread.sleep((int)(Math.random()*(X-Y+1)+Y));
            this.datagramSocket.send(datagramPacket);
            comandoHelp();
            // Se muestra por consola el mensaje de distribucion.
            System.out.println("SEND: " + new String(datagramPacket.getData()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}