package App.Server;

import java.io.IOException;
import java.net.*;
import java.util.*;

/**
 * Servidor
 */
public class Servidor {

    // Tamaño de mensaje UDP.
    public transient static final int MAX_UDP_PACKET_LENGTH = 1450;

    // Tiempo del TimeOut.
    public transient static final int TIME_OUT = 2 * 1000;


    // Bridge del analizador
    protected transient Abstracion abstracion;

    public Servidor() {
        // Por defecto el analizador es XML.
        abstracion = new AbstracionRedefinida(new AnalizadorXML());
    }

    protected List<InetAddress> listAllBroadcastAddresses() throws SocketException {
        List<InetAddress> broadcastList = new ArrayList<>();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();
            if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                continue;
            }
            networkInterface.getInterfaceAddresses().stream().map(InterfaceAddress::getBroadcast)
                    .filter(Objects::nonNull).forEach(broadcastList::add);
        }
        return broadcastList;
    }


    protected void enviarMensajeBroadcast(String stringDatos, DatagramSocket datagramSocket) {
        try {

            // Se envia los datos por los puertos de broadcast.
            byte[] buffer = stringDatos.getBytes();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length,
                    InetAddress.getByName(listAllBroadcastAddresses().get(0).getCanonicalHostName()), 4445);

            new ServidorThread(datagramSocket, packet).start();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    protected void recibirMensajeCliente(DatagramSocket datagramSocket) {

        byte[] buffer = new byte[MAX_UDP_PACKET_LENGTH];
        DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
        String stringDatos;
        try {
            datagramSocket.setSoTimeout(TIME_OUT);
            // Recibir datos del cliente.
            datagramSocket.receive(receivePacket);
            stringDatos = new String(receivePacket.getData());
            System.out.println("Recibe: " + stringDatos + " del puerto cliente: " + receivePacket.getPort());

            // Enviar respuesta al cliente.
            byte[] sendData = "Enviamos Cosas".getBytes();
            InetAddress inetAddress = receivePacket.getAddress();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, inetAddress, receivePacket.getPort());
            datagramSocket.send(sendPacket);
        } catch (SocketTimeoutException e) {
            System.err.println("TimeOut: Not data input");
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void cambiarCodificacion(String codificacionString) {
        switch (codificacionString.toLowerCase()) {
            case "xml":
                abstracion = new AbstracionRedefinida(new AnalizadorXML());
                break;

            case "json":
                abstracion = new AbstracionRedefinida(new AnalizadorJSON());
                break;
        }
    }

}