package App.Server;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import com.google.gson.Gson;

/**
 * AnalizadorJson
 */
public class AnalizadorJSON implements Implementador {

	private static final String SERIALIZED_FILE = "src/main/java/App/DataBase/datosDistribucion.json";

	@Override
	public void operacion() {
		System.out.println("Analizador Json");
	}

	@Override
	public String serializar(Object object) {
		String jsonDatos = new Gson().toJson(object);
		try {
			FileWriter fileWriter = new FileWriter(SERIALIZED_FILE, true);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			printWriter.println(jsonDatos);
			printWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return jsonDatos;

	}

	@Override
	public Object deserializar(Object object, String stringDatos) {
		Gson gson = new Gson();
		Object serverObject = gson.fromJson(stringDatos, object.getClass());
		System.out.println(serverObject.toString());
		return serverObject;
	}

	@Override
	public String crearMensajeDistribucion(Object object, String stringDatos) {
		return new String("Server: " + object.getClass().getSimpleName() + " JSON: " + stringDatos);
	}

	@Override
	public String crearMensajeControl(Object object, String stringDatos) {
		return null;
	}

	// No esta implementado
	@Override
	public void validacion(String stringDatos) {
	}

}