package App.Server;

/**
 * Abstracion
 */
public interface Abstracion {

	public void operacion();

	public void validacion(String stringDatos);

	public String serializar(Object object);

	public Object deserializar(Object object, String stringDatos);

	public String crearMensajeDistribucion(Object object, String stringDatos);

	public String crearMensajeControl(Object object, String stringDatos);

}