package App.Server;

import java.io.File;
import java.io.FileWriter;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.XMLConstants;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;


/**
 * AnalizadorXML
 */
public class AnalizadorXML implements Implementador {

	private static final String SERIALIZED_FILE = "src/main/java/App/DataBase/datosDistribucion.xml";
	private static final String SCHEMA_FILE = "src/main/java/App/DataBase/datosDistribucionSchema.xml";

	@Override
	public void operacion() {
		System.out.println("Analizador XML");
	}

	@Override
	public String serializar(Object object) {

		// Creamos el StirngWrites para poder devolver el XML en forma de string
		StringWriter stringWriter = new StringWriter();
		try {
			// Abrimos el fichero, en modo append
			FileWriter fileWriter = new FileWriter(SERIALIZED_FILE, true);
			JAXBContext jaxbContent = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = jaxbContent.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			marshaller.marshal(object, fileWriter);
			marshaller.marshal(object, stringWriter);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return stringWriter.toString();
	}

	// TODO Después de crear el String del XML(que es lo que voy a enviar)
	// lo valido y lo añado al fichero de base de datos.
	@Override
	public Object deserializar(Object object, String stringDatos) {
		StringReader stringReader = new StringReader(stringDatos);
		Object server = null;
		// Nombre de la clase(del paquete)
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			server = unmarshaller.unmarshal(stringReader);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return server;

	}


	@Override
	public void validacion(String stringDatos) {
		try {
			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File(SCHEMA_FILE));
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new StringReader(stringDatos)));

		} catch (Exception e) {
			System.err.println("Fallo al validar el xml");
		}
	}

	@Override
	public String crearMensajeDistribucion(Object object, String stringDatos) {
		return new String("Server: " + object.getClass().getSimpleName() + " XML: " + stringDatos);

	}

	@Override
	public String crearMensajeControl(Object object, String stringDatos) {
		return null;
	}

}