package App.Server;

/**
 * Implementador
 */

public interface Implementador {
	void operacion();

	void validacion(String stringDatos);

	String serializar(Object object);

	Object deserializar(Object object, String stringDatos);
	
	String crearMensajeDistribucion(Object object, String stringDatos);

	String crearMensajeControl(Object object, String stringDatos);

}
