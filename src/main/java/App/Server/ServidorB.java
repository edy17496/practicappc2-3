package App.Server;

import java.io.IOException;
import java.net.*;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.*;

/**
 * ServidorB
 */
@XmlRootElement(name = "servidorB")
public class ServidorB extends Servidor {

	public transient static final int PORT = 4447;

	/**
	 * La velocidad se puede representar tanto en km/h como en ms/s.
	 */
	@XmlAttribute(name = "velicidadViento")
	private List<Integer> velicidadViento;

	@XmlAttribute(name = "pluviometria")
	private List<Integer> pluviometria;

	@XmlAttribute(name = "probabilidadLluvia")
	private List<Integer> probabilidadLluvia;

	private DatagramSocket datagramSocket;

	public ServidorB() {

		try {
			// Datagram Socket del ServidorA.
			this.datagramSocket = new DatagramSocket(PORT);
		} catch (SocketException e) {
			e.printStackTrace();
		}

		pluviometria = new LinkedList<>();
		velicidadViento = new LinkedList<>();
		probabilidadLluvia = new LinkedList<>();

	}

	public void setDatos() {

		// Eliminamos los datos anteriores.
		pluviometria.removeAll(pluviometria);
		velicidadViento.removeAll(velicidadViento);
		probabilidadLluvia.removeAll(probabilidadLluvia);

		// Se crean los nuevos datos.
		for (int i = 0; i <= 10; i++) {
			pluviometria.add((int) (Math.random() * 50));
			velicidadViento.add((int) (Math.random() * 50));
			probabilidadLluvia.add((int) (Math.random() * 100));
		}
	}

	public void enviarMensajeBroadcast() {
		// Se crea el mensaje con los nuevos datos.
		this.setDatos();

		// Se serializa los datos del ServidorA y se crea el mensaje de distribucion.
		String xmlString = abstracion.serializar(this);
		String mensaje = abstracion.crearMensajeDistribucion(this, xmlString);
		super.enviarMensajeBroadcast(mensaje, datagramSocket);

	}

	public void recibirMensajeCliente() {
		super.recibirMensajeCliente(datagramSocket);
	}


	public void mainServidorB() {
		String opcion = "";
		System.out.println("Inicio ServidorB ");

		while (!opcion.equals("exit")) {
			// Envio de datos del ServidorA al broadcast.
			enviarMensajeBroadcast();

			// Recibir mensajes del cliente.
			recibirMensajeCliente();
		}
		System.out.println("Fin ServidorB");
		this.datagramSocket.close();
	}

	@Override
	public String toString() {
		return "ServidorB{ Pluviometria" + pluviometria + " " + "VelicidadViento" + velicidadViento + " "
				+ "ProbabilidadLluvia" + probabilidadLluvia + " }";
	}

	public static void main(String[] args) {
		ServidorB servidorB = new ServidorB();
		servidorB.mainServidorB();
	}

}