package App.Server;

import App.Server.Abstracion;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		System.out.println("Edy1");
		Abstracion abstracion;
		ServidorB servidor;
		Scanner scanner = new Scanner(System.in);
		String opcion = " ";
		String mensajeDistribucion;

		while (!opcion.equals("exit")) {
			System.out.println("XML JSON");
			opcion = scanner.next();
			servidor = new ServidorB();

			if (opcion.equals("xml")) {
				// Se crea el analizador
				abstracion = new AbstracionRedefinida(new AnalizadorXML());

				// Serializar el objeto.
				String xmlString = abstracion.serializar(servidor);

				// Validamos el mensaje.
				abstracion.validacion(xmlString);

				// Creamos el mensaje de distribucion
				mensajeDistribucion = abstracion.crearMensajeDistribucion(servidor, xmlString);
				System.out.println(mensajeDistribucion);

				// Deserealizar el xml
				abstracion.deserializar(servidor, xmlString);
			}
			if (opcion.equals("json")) {
				// Se crea el analizador
				abstracion = new AbstracionRedefinida(new AnalizadorJSON());
				String jsonString = abstracion.serializar(servidor);

				// Creamos el mensaje de distribucion
				mensajeDistribucion = abstracion.crearMensajeDistribucion(servidor, jsonString);
				System.out.println(mensajeDistribucion);

				// Deserealizar json
				abstracion.deserializar(servidor, jsonString);

			}

		}
		scanner.close();

	}

}