package App.Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import javax.xml.bind.annotation.*;

/**
 * ServidorA
 */

@XmlRootElement(name = "servidorA")
public class ServidorA extends Servidor {

    public transient static final int PORT = 4446;

    /**
     * La temperatura se puede representa tanto grados Kelvin como grados Celcius.
     */
    @XmlAttribute(name = "temperatura")
    private List<Integer> temperatura;

    @XmlAttribute(name = "humedad")
    private List<Integer> humedad;

    @XmlAttribute(name = "direccionViento")
    private List<DireccionViento> direccionViento;

    private DatagramSocket datagramSocket;

    private Scanner scanner;

    public ServidorA() {
        try {
            // Datagram Socket del ServidorA.
            this.datagramSocket = new DatagramSocket(PORT);
        } catch (SocketException e) {
            e.printStackTrace();
        }

        // Datos del servidor.
        temperatura = new LinkedList<>();
        humedad = new LinkedList<>();
        direccionViento = new LinkedList<>();

        // Scanner para los comandos del servidor.
        scanner = new Scanner(System.in);

    }

    public void setDatos() {

        // Eliminamos los datos anteriores.
        temperatura.removeAll(temperatura);
        humedad.removeAll(humedad);
        direccionViento.removeAll(direccionViento);

        // Se crean los nuevos datos.
        for (int i = 0; i <= 10; i++) {
            temperatura.add((int) (Math.random() * 50));
            humedad.add((int) (Math.random() * 50));
            direccionViento.add(DireccionViento.values()[new Random().nextInt(DireccionViento.values().length)]);
        }
    }

    public void enviarMensajeBroadcast() {
        // Se crea el mensaje con los nuevos datos.
        this.setDatos();

        // Se serializa los datos del ServidorA y se crea el mensaje de distribucion.
        String xmlString = abstracion.serializar(this);
        String mensaje = abstracion.crearMensajeDistribucion(this, xmlString);
        super.enviarMensajeBroadcast(mensaje, datagramSocket);

    }

    @Override
    public void cambiarCodificacion(String datosString) {
        super.cambiarCodificacion(datosString);
    }

    public void recibirMensajeCliente() {
        super.recibirMensajeCliente(datagramSocket);
    }

    public void mainServidorA() {
        String opcion = "";
        System.out.println("Inicio ServidorA ");

        while (!opcion.equals("exit")) {
            // Envio de datos del ServidorA al broadcast.
            enviarMensajeBroadcast();

            // Recibir mensajes del cliente.
            recibirMensajeCliente();
        }
        System.out.println("Fin ServidorA");
        this.datagramSocket.close();
    }

    @Override
    public String toString() {
        return "ServidorA{ Temperatura" + temperatura + " " + "Humedad" + humedad + " " + "DireccionViento"
                + direccionViento + " }";
    }

    public static void main(String[] args) {
        ServidorA servidorA = new ServidorA();
        servidorA.mainServidorA();
    }

}