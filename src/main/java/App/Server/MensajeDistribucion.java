package App.Server;

/**
 * MensajeDistribucion, patrón para que dependa el tipo de mensaje a crear
 */
public class MensajeDistribucion {

	private String server;
	private String xmlString;

	public MensajeDistribucion(Object server, String xmlString) {
		this.server = server.getClass().getSimpleName().toString();
		this.xmlString = xmlString;
	}

	public String createMensajeDistribucion() {
		return new String("Server: " + this.server + " XML: " + this.xmlString);
	}

	/**
	 * @return the server
	 */
	public String getServer() {
		return server;
	}

	/**
	 * @return the xmlString
	 */
	public String getXmlString() {
		return xmlString;
	}

}