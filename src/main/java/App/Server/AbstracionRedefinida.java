package App.Server;

/**
 * AbstracionRedefinida
 */
public class AbstracionRedefinida implements Abstracion {

	private Implementador implementador;

	public AbstracionRedefinida(Implementador implementador) {
		this.implementador = implementador;
	}

	@Override
	public void operacion() {
		implementador.operacion();
	}

	@Override
	public void validacion(String stringDatos) {
		implementador.validacion(stringDatos);
	}

	@Override
	public String serializar(Object object) {
		return implementador.serializar(object);
	}

	@Override
	public Object deserializar(Object object, String stringDatos) {
		return implementador.deserializar(object, stringDatos);
	}

	@Override
	public String crearMensajeDistribucion(Object object, String stringDatos) {
		return implementador.crearMensajeDistribucion(object, stringDatos);
	}

	@Override
	public String crearMensajeControl(Object object, String stringDatos) {
		return implementador.crearMensajeControl(object, stringDatos);
	}
}