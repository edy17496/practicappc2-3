package App.Client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

import App.Server.Servidor;

/**
 * Cliente
 */
public class Cliente {

    private DatagramSocket datagramSocket;
    private InetAddress inetAddress;
    private Scanner scanner;
    private Map<String, Integer> puertoServer;

    public Cliente() {
        try {
            this.datagramSocket = new DatagramSocket(4445);
            this.inetAddress = InetAddress.getByName(listAllBroadcastAddresses().get(0).getCanonicalHostName());
            scanner = new Scanner(System.in);
            puertoServer = new HashMap<>();

        } catch (SocketException | UnknownHostException e) {
            e.printStackTrace();
        }

    }

    /**
     * @return the puertoServer
     */
    public Map<String, Integer> getPuertoServer() {
        return puertoServer;
    }

    public List<InetAddress> listAllBroadcastAddresses() throws SocketException {
        List<InetAddress> broadcastList = new ArrayList<>();
        Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
        while (interfaces.hasMoreElements()) {
            NetworkInterface networkInterface = interfaces.nextElement();
            if (networkInterface.isLoopback() || !networkInterface.isUp()) {
                continue;
            }
            networkInterface.getInterfaceAddresses().stream().map(InterfaceAddress::getBroadcast)
                    .filter(Objects::nonNull).forEach(broadcastList::add);
        }
        return broadcastList;
    }

    public void comandoHelp() {
        System.out.print("COMANDOS A UTILIZAR\n" + "\tINICIAR consultas de datos al broadcast [START]\n"
                + "\tDETENER consultas de datos al broadcast [STOP]\n"
                + "\tHELP interactuar con los servidores [HELP]\n" + "\tSERIELIZADOR de los servidores [JSON/XML]\n"
                + "\tSalir de la aplicación [EXIT]\n");
    }

    public void enviarMensaje(String datosString, int portServer) {
        try {
            // Enviamos la peticion al servidor
            // Generamos los datos de control para ser enviados por parte del Cliente
            byte[] buffer = datosString.getBytes();
            DatagramSocket datagramSocket = new DatagramSocket();
            DatagramPacket packet = new DatagramPacket(buffer, buffer.length, inetAddress, portServer);
            datagramSocket.send(packet);

            // Recibimos la peticion del servidor.
            byte[] bufer = new byte[Servidor.MAX_UDP_PACKET_LENGTH];
            DatagramPacket respuesta = new DatagramPacket(bufer, bufer.length);
            datagramSocket.receive(respuesta);
            String datosRecibidos = new String(respuesta.getData());
            System.out.println("Recibido: " + datosRecibidos);
            datagramSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String elegirOpcion() {
        System.out.print("> ");
        return scanner.nextLine().toLowerCase();
    }

    public void mainCliente() {
        String opcion;
        int puertoServidor;
        ClienteThread clienteThread = null;

        comandoHelp();
        opcion = elegirOpcion();
        while (!opcion.equals("exit")) {
            switch (opcion) {
                // Recibir mensajes del broadcast.
                case "start":
                    clienteThread = new ClienteThread(this, datagramSocket);
                    clienteThread.start();
                    System.out.println(clienteThread.getId());
                    opcion = elegirOpcion();
                    break;
                // Detener la recepcion de mensajes del broadcast.
                case "stop":
                    if (clienteThread != null) {
                        clienteThread.setActivo(false);
                        System.out.println("Final conexion con broadcast");
                    }
                    else {
                        System.out.println("No se ha realizado ninguna peticion");
                    }
                    opcion = elegirOpcion();
                    break;
                case "help":
                    System.out.println("Elegir un servidor [A | B]");
                    puertoServidor = puertoServer.get(new Scanner(System.in).nextLine().toLowerCase());
                    System.out.println("Puerto del servidor: " + puertoServidor);
                    break;
                default:
                    System.out.println("Comando no valido");
                    comandoHelp();
                    opcion = elegirOpcion();
                    break;
            }
        }
    }

    public static void main(String[] args) {
        Cliente cliente = new Cliente();
        System.out.println("INICIO CLIENTE");
        cliente.mainCliente();
        System.out.println("FIN CLIENTE");
    }
}