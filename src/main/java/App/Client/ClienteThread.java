package App.Client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import App.Server.Servidor;

/**
 * ClienteThread
 */
public class ClienteThread extends Thread {
	private DatagramSocket datagramSocket;
	private Cliente cliente;
	private boolean activo;

    public ClienteThread(Cliente cliente, DatagramSocket datagramSocket) {
		this.datagramSocket = datagramSocket;
		this.cliente = cliente;
		this.activo = true;
	}

	public void setActivo(boolean activo) {
		this.activo = activo;
	}

	@Override
	public void run() {
		while(activo){
			recibirMensajes();
		}
	}

	public void recibirMensajes() {
		byte[] buffer = new byte[Servidor.MAX_UDP_PACKET_LENGTH];
		String datosRecibidos;
		int puerto;

		try {
			Thread.sleep(2500);
			DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
			this.datagramSocket.receive(receivePacket);
			datosRecibidos = new String(receivePacket.getData());
			puerto = receivePacket.getPort();
			// Se trata del ServidorA
			if (datosRecibidos.contains("ServidorA")) {
				this.cliente.getPuertoServer().put("a", puerto);
			} // Se trata de ServidorB
			else if (datosRecibidos.contains("ServidorB")) {
				this.cliente.getPuertoServer().put("b", puerto);
			}
			System.out.println(datosRecibidos);
		} catch (IOException | InterruptedException e) {

			e.printStackTrace();
		}
	}

}